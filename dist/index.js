import { ApolloServer } from '@apollo/server';
import { startStandaloneServer } from '@apollo/server/standalone';
import fetch from 'node-fetch';
const typeDefs = `#graphql
    type Query {
        pokemon(name: String!): Pokemon
        listPokemon: [PokemonListItem]
    }
    type Pokemon {
        id: Int!
        name: String!
        base_experience: Int
        height: Int
        weight: Int
    }
    type PokemonListItem {
        name: String!
        url: String!
    }
`;
const resolvers = {
    Query: {
        pokemon: async (parent, args) => {
            const response = await fetch(`https://pokeapi.co/api/v2/pokemon/${args.name}`);
            const { id, name, base_experience, height, weight } = await response.json();
            return {
                id,
                name,
                base_experience,
                height,
                weight,
            };
        },
        listPokemon: async () => {
            const response = await fetch(`https://pokeapi.co/api/v2/pokemon?limit=151`);
            const { results } = await response.json();
            return results;
        },
    },
};
const server = new ApolloServer({
    typeDefs,
    resolvers,
});
const { url } = await startStandaloneServer(server, {
    listen: { port: 4000 },
});
console.log(`🚀  Server ready at: ${url}`);
